# Wordle

Wordle is a word guessing game that challenges players to find a secret word by making guesses and receiving feedback on the presence of correct letters. In our version, we include numerous features such as hint management and score saving. We also parse https://en.wiktionary.org/wiki/Wiktionary:Frequency_lists/French_wordlist_1750 to obtain a database of our words.

## Installation

1. Clone the repository.

2. Install Python dependencies via a terminal:

- pip install gensim
- pip install scikit-learn
- pip install flask
- pip install python-Levenshtein
- pip install unidecode

3. Install Java and JavaFX

- [Download Eclipse](https://www.eclipse.org/downloads/)
- [Download JavaFX](https://openjfx.io/)

## Usage

1. Run the test HTTP server with `python word2vecmodel.py`.
2. Launch the application by executing the Main directly in Eclipse.
3. Modify the path 'cheminDictionnaireLocal' of the dictionnary by your own in the class Wordle.java.

## Authors

- @akram_bourouina
- @rayanegeyer
- @Mehdi_MM2
- @Mejai_Wajdi

## License

This project is developed within the framework of the Programming Project 1 course at the University of Avignon.

## Project Status

Archived

## Note

This is not a deployed final version. The scope of our project is limited to local development of Wordle.
